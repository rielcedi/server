/*
 * chat_client.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 59633 // ポート番号

int main(int argc, char *argv[])
{
  struct sockaddr_in addr;
  int fd;
  int buflen;
  char buf[1024];

  if (argc != 2)
  {
    fprintf(stderr, "Usage: %s <IP Address>\n", argv[0]);
    exit(1);
  }

  /* ソケット作成 */
  if ((fd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("*** client: socket ***");
    exit(1);
  }

  bzero((char *)&addr, sizeof(addr));
  addr.sin_family = PF_INET;
  addr.sin_addr.s_addr = inet_addr(argv[1]);
  addr.sin_port = htons(PORT);

  /* ソケット接続要求 */
  if (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
  {
    perror("*** client: connect ***");
    close(fd);
    exit(1);
  }
  printf("Connected to server.\n");
  printf("Input 'quit' to disconnect...\n");

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /* データの送受信 */

  /*//確かめ：文字列inoutをサーバーに送る。
    strcpy(buf,"inout");
    buf[strlen(buf)]='\0';
    write( fd, buf, strlen(buf)*sizeof(char));
    //writeの3番目の引数はバイト数を表す。sizeof(buf)だとbufの長さbuf[1024]のバイト数を返す。
    */

  while (1)
  {

    //bufにはサーバーから送られてきたコマンドが格納されている。
    //keijiban:掲示板の過去ログを表示
    //comment:入力されたコメントを過去ログに格納
    //update:最新の過去ログをユーザー側に表示
    //quit:掲示板の通信を終了

    //ユーザーから送られてきたデータがbufに格納される。
    buflen = read(0, buf, sizeof(buf) - 1);
    //buf[buflen] = '\0';

    //keijibanコマンドが入力されたとき
    if (strcmp(buf, "keijiban\n") == 0)
    {

      //ユーザーにスレッドを選択させたい。
      //スレッド管理プログラムを呼んでスレッド一覧を表示させる。
      strcpy(buf, "thread");
      //buf[strlen(buf)]='\0';
      write(/*スレッド管理プログラムとの通信を表すfd*/, buf, strlen(buf) * sizeof(char));
      //スレッド管理プログラムからスレッド一覧を表示する。
      while (1)
      {
        //スレッド管理ファイルからスレッドをbufで受け取る。
        buflen = read(0, buf, sizeof(buf) - 1);
        buf[buflen] = '\0';

        //ユーザーにbuf(スレッド名が格納されている)を表示する。
        write(/*ユーザーとの通信を表すfd*/, buf, sizeof(buf));

        if (strcmp(buf, "thread_q\n") == 0)
        {
          break; //スレッドを表示し終えたのでスレッドの表示を終了する。
        }
      }

      //ユーザーにスレッドを選ばせる。
      strcpy(buf, "please choose thread.\n");
      buf[strlen(buf)] = '\0';
      write(/*ユーザーとの通信を表すfd*/, buf, strlen(buf) * sizeof(char));

      //ユーザーが選んだスレッドを受け取る。
      buflen = read(0, buf, sizeof(buf) - 1);
      buf[buflen] = '\0';

      //選ばれたスレッドを読み込みプログラムに送る。
      write(/*スレッド管理プログラムとの通信を表すfd*/, buf, sizeof(buf));

      //読み込みプログラムに最新50個を出力するように伝える。
      for (i = ; i <= 50; i++)
      {

        //bufに読み込んでほしいコメントの番号(つまりi)を格納する。

        //読み込みプログラムに読み込んでほしいコメントの番号を送る。
        write(/*よみこみプログラムとの通信を表すfd*/, buf, sizeof(buf));

        //読み込みプログラムからコメントの文字列を受け取る。
        buflen = read(0, buf, sizeof(buf) - 1);
        buf[buflen] = '\0';

        //受け取った文字列bufをユーザー側に送る。(表示する)
        write(/*ユーザー側との通信を表すfd*/, buf, sizeof(buf));
      }

      //掲示板の使い方(コマンドの種類)をユーザーに伝える。
      strcpy(buf, "please choose command in [add_thread/comment/update/quit]\n");
      buf[strlen(buf)] = '\0';
      write(/*ユーザーとの通信を表すfd*/, buf, strlen(buf) * sizeof(char));

      //keijibanの処理を行う。
      //comment:update:quitどれかのコマンドが送られてくる。
      //while(1)内でそれぞれの処理を行う。
      while (1)
      {

        //ユーザーから送られてきたコマンドをbufに格納する。
        buflen = read(0, buf, sizeof(buf) - 1);
        buf[buflen] = '\0';

        //add_threadコマンドが入力された場合
        if (strcmp(buf, "add_thread\n") == 0)
        {

          //新しいスレッドの名前をユーザーに書かせるように説明を送る。（日本語禁止）
          strcpy(buf, "please write new thread name.\n");
          buf[strlen(buf)] = '\0';
          write(/*ユーザーとの通信を表すfd*/, buf, strlen(buf) * sizeof(char));

          //ユーザーが書いた新しいスレッド名を受け取る。
          buflen = read(0, buf, sizeof(buf) - 1);
          buf[buflen] = '\0';

          //このスレッド名をスレッド管理プログラムに送る。
          write(/*スレッド管理プログラムとの通信を表すfd*/, buf, strlen(buf) * sizeof(char));
        }

        //commentコマンドが入力された場合
        if (strcmp(buf, "comment\n") == 0)
        {

          //以下にコメントを書いてくださいと伝える。
          strcpy(buf, "↓ please write your comment. ↓\n");
          buf[strlen(buf)] = '\0';
          write(/*ユーザーとの通信を表すfd*/, buf, strlen(buf) * sizeof(char));

          //ユーザーが書き込んだ内容をbufに格納する。
          buflen = read(0, buf, sizeof(buf) - 1);
          buf[buflen] = '\0';

          //過去ログに格納するプログラムを呼び出して、bufを渡す。
          write(/*格納プログラムとの通信を表すfd*/, buf, strlen(buf) * sizeof(char));

          //格納プログラムから書き込んだとの確認信号を受けてこの処理は終了する。
          if (strcmp(buf, "ok") == 0)
          {
            break;
          }
          else
          {
            //過去ログへの書き込みを失敗したのでユーザー側にエラーを返す。
            strcpy(buf, "ERROR:cannot write in the past log.\n");
            buf[strlen(buf)] = '\0';
            write(/*ユーザーとの通信を表すfd*/, buf, strlen(buf) * sizeof(char));
          }
        }

        //updateコマンドが入力された場合
        if (strcmp(buf, "update\n") == 0)
        {

          //updateを要求したユーザーの最新番号を取得する。

          for (i = /*そのユーザーの最新番号*/; i <= /*過去ログの最新番号*/; i++)
          {

            //bufに読み込んでほしいコメントの番号(つまりi)を格納する。

            //読み込みプログラムに読み込んでほしいコメントの番号を送る。
            write(/*よみこみプログラムとの通信を表すfd*/, buf, sizeof(buf));

            //読み込みプログラムからコメントの文字列を受け取る。
            read(0, buf, sizeof(buf));

            //受け取った文字列bufをユーザー側に送る。(表示する)
            write(/*ユーザー側との通信を表すfd*/, buf, sizeof(buf));
          }
        }

        //quitコマンドが入力された場合
        if (strcmp(buf, "quit\n") == 0)
        {
          break;
        }
      }
    }
  }

  /* ソケット切断 */
  exit(close(fd));
}
