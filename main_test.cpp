/*
 * main.cpp
 * 
 * 未
 * タイムアウトさせるか？
 * 接続時にlistening_fdをcloseすべきではないのか？
 * ↑一度closeされたものは再度listenしても正しく接続できない
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <vector>
#include <string>
using namespace std;

#ifdef __linux
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#ifdef _WIN32
#include <winsock2.h>
/*
compile 
g++ -lwsock32 main_test.cpp -lws2_32 -o server.exe
*/
#endif

class Main_server
{
  public:
    Main_server(int cl_num, int *port_num)
    {
        CLIENT_NUM = cl_num;
        PORT = port_num;

        server_addr = new struct sockaddr_in[CLIENT_NUM];
        client_addr = new struct sockaddr_in[CLIENT_NUM];
        connected_fd = new int[CLIENT_NUM];
        listening_fd = new int[CLIENT_NUM];
        wait_set_fd = new int[CLIENT_NUM * 2];   //max  connected_fd+listening_fd
        wait_clear_fd = new int[CLIENT_NUM * 2]; //max  connected_fd+listening_fd
        st = new enum Status[CLIENT_NUM];
        list_zero();
        Setstatus(UNUSED);
#ifdef _WIN32
/*
        if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0)
        {
            printf("error\n");
            exit(1);
        }*/
#endif
    }
    ~Main_server()
    {
        Setstatus(UNUSED);
        //delete[] PORT;
        delete[] server_addr;
        delete[] client_addr;
        delete[] connected_fd;
        delete[] listening_fd;
        delete[] wait_set_fd;
        delete[] wait_clear_fd;
        delete[] st;
    }

  protected:
    int *PORT; /* ポート番号 */
    struct sockaddr_in *server_addr;
    struct sockaddr_in *client_addr;
    int len;
    int CLIENT_NUM;
    int *connected_fd, *listening_fd;
    int *wait_set_fd, *wait_clear_fd;
    enum Status
    {
        UNUSED,    //未使用
        LISTENING, //接続待ち
        CONNECTED  //接続中
    } * st;
#ifdef _WIN32
    // WSADATA wsaData;
#endif
  public:
    void make_socket(int num)
    {
        /* リスニングソケット作成 */
        if ((listening_fd[num] = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("*** server: socket ***");
            exit(1);
        }

#ifdef __linux
        int reuse = 1;
        setsockopt(listening_fd[num], SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int));
#endif
#ifdef _WIN32
        int reuse = 1;
        setsockopt(listening_fd[num], SOL_SOCKET, SO_REUSEADDR, (const char *)&reuse, sizeof(int));
#endif

        /* アドレスファミリ・ポート番号・IPアドレス設定 */
        memset(&server_addr[num], 0, sizeof(server_addr[num]));
        server_addr[num].sin_family = PF_INET;
        server_addr[num].sin_addr.s_addr = htonl(INADDR_ANY);
        server_addr[num].sin_port = htons(PORT[num]);

        /* リスニングソケットにアドレスを割り当て */

        if (bind(listening_fd[num], (struct sockaddr *)&server_addr[num], sizeof(server_addr[num])) < 0)
        {
            perror("*** server: bind ***");
            close(listening_fd[num]);
            exit(1);
        }
    }
    void Listening_socket(void)
    {
        int i;

        for (i = 0; i < CLIENT_NUM; i++)
        {
            if (Getstatus(i) == UNUSED)
            {
                make_socket(i);
                wait_set_list(listening_fd[i]); //FD_SET(listening_fd[i], &readfds);
                Setstatus(i, LISTENING);
            }
        }
        /* リスニングソケット待ち受け */
        for (i = 0; i < CLIENT_NUM; i++)
        {
            if (Getstatus(i) == LISTENING)
            {
                if (listen(listening_fd[i], 1) < 0)
                {
                    perror("*** server: listen ***");
                    close(listening_fd[i]);
                    exit(1);
                }
            }
        }
    }
    void Listening_socket(int num)
    {

        if (Getstatus(num) == UNUSED)
        {
            make_socket(num);
            wait_set_list(listening_fd[num]); //FD_SET(listening_fd[num], &readfds);
            Setstatus(num, LISTENING);
        }

        /* リスニングソケット待ち受け */
        if (Getstatus(num) == LISTENING)
        {
            if (listen(listening_fd[num], 1) < 0)
            {
                perror("*** server: listen ***");
                close(listening_fd[num]);
                exit(1);
            }
        }
    }
    void connect_server(int i)
    {

        /* 接続要求受け付け */
        printf("num=%d\n", PORT[i]);

        len = sizeof(client_addr[i]);
#ifdef _WIN32
        connected_fd[i] = accept(listening_fd[i], (struct sockaddr *)&client_addr[i], &len);
#endif
#ifdef __linux
        connected_fd[i] = accept(listening_fd[i], (struct sockaddr *)&client_addr[i], (socklen_t *)&len);
#endif

        if (connected_fd[i] < 0)
        {
            perror("*** server: accept ***");
            for (int j = 0; j < CLIENT_NUM; j++)
            {
                close(listening_fd[i]);
                wait_clear_list(listening_fd[i]); //FD_CLR(listening_fd[i], &readfds);
            }
            exit(1);
        }

        //close(listening_fd[i]); //切断後再接続不可能？
        Setstatus(i, CONNECTED);
        wait_clear_list(listening_fd[i]); //FD_CLR(listening_fd[i], &readfds);
        wait_set_list(connected_fd[i]);   //FD_SET(connected_fd[i], &readfds);
        printf("Accepted connection.\n");
    }
    int close_server(int num)
    {
        if (num >= CLIENT_NUM) //error
            return -1;
        printf("close\n");
        /* ソケット切断 */
        wait_clear_list(connected_fd[num]); //FD_CLR(connected_fd[num], &readfds);
        close(connected_fd[num]);
        Setstatus(num, LISTENING);
        wait_set_list(listening_fd[num]); //FD_SET(listening_fd[num], &readfds);
        return 0;
    }

    void Setstatus(int num, enum Status s)
    {
        st[num] = s;
    }
    void Setstatus(enum Status s)
    {
        for (int num = 0; num < CLIENT_NUM; num++)
        {
            Setstatus(num, s);
        }
    }
    enum Status Getstatus(int num)
    {
        return st[num];
    }
    void wait_set_list(int fd)
    {
        int i = 0;
        while (wait_set_fd[i] != 0)
            i++;

        wait_set_fd[i] = fd;
    }
    void wait_clear_list(int fd)
    {
        int i = 0;
        while (wait_clear_fd[i] != 0)
            i++;

        wait_clear_fd[i] = fd;
    }
    void list_zero()
    {
        int i = 0;
        for (int i = 0; i < CLIENT_NUM; i++)
        {
            listening_fd[i] = 0;
            connected_fd[i] = 0;
        }
        for (int i = 0; i < CLIENT_NUM * 2; i++)
        {

            wait_set_fd[i] = 0;
            wait_clear_fd[i] = 0;
        }
    }
};
class Main_server_user
{
  public:
    Main_server_user(int cl_num, int port_num)
    {
        CLIENT_NUM = cl_num;
        PORT = port_num;
        //server_addr = new struct sockaddr_in[CLIENT_NUM];
        client_addr = new struct sockaddr_in[CLIENT_NUM];
        connected_fd = new int[CLIENT_NUM];
        //listening_fd = new int[CLIENT_NUM];
        wait_set_fd = new int[CLIENT_NUM + 1];   //max  connected_fd+listening_fd
        wait_clear_fd = new int[CLIENT_NUM + 1]; //max  connected_fd+listening_fd
        st = new enum Status[CLIENT_NUM];
        list_zero(); //FD_ZERO(&readfds);
        Setstatus(UNUSED);
#ifdef _WIN32
        /*  if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0)
        {
            printf("error\n");
            exit(1);
        }*/
#endif
    }
    ~Main_server_user()
    {
        Setstatus(UNUSED);
        //delete[] server_addr;
        delete[] client_addr;
        delete[] connected_fd;
        //delete[] listening_fd;
        delete[] wait_set_fd;
        delete[] wait_clear_fd;
        delete[] st;
    }

  protected:
    int maxfd;
    int PORT; /* ポート番号 */
    struct sockaddr_in server_addr;
    struct sockaddr_in *client_addr;
    int len;
    int CLIENT_NUM;
    int *connected_fd, listening_fd;
    int *wait_set_fd, *wait_clear_fd;
    enum Status
    {
        UNUSED,    //未使用
        LISTENING, //接続待ち
        CONNECTED  //接続中
    } * st;
#ifdef _WIN32
    //  WSADATA wsaData;
#endif
  public:
    void make_socket()
    {
        /* リスニングソケット作成 */
        if ((listening_fd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("*** server: socket ***");
            exit(1);
        }

#ifdef __linux
        int reuse = 1;
        setsockopt(listening_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int));
#endif
#ifdef _WIN32
        int reuse = 1;
        setsockopt(listening_fd, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuse, sizeof(int));
#endif

        /* アドレスファミリ・ポート番号・IPアドレス設定 */
        memset(&server_addr, 0, sizeof(server_addr));
        server_addr.sin_family = PF_INET;
        server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        server_addr.sin_port = htons(PORT);

        /* リスニングソケットにアドレスを割り当て */

        if (bind(listening_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
        {
            perror("*** server: bind ***");
            close(listening_fd);
            exit(1);
        }
    }
    void Listening_socket_init(void)
    {
        make_socket();
        wait_set_list(listening_fd); //FD_SET(listening_fd, &readfds);
        if (listen(listening_fd, 1) < 0)
        {
            perror("*** server: listen ***");
            close(listening_fd);
            exit(1);
        }
        find_unused_fd();
    }
    void find_unused_fd(void)
    {
        int i;

        for (i = 0; i < CLIENT_NUM; i++)
        {
            if (Getstatus(i) == UNUSED)
            {
                printf("%d\n", i);
                Setstatus(i, LISTENING);
                break;
            }
        }
    }
    void connect_server(int i)
    {

        /* 接続要求受け付け */
        printf("num=%d\n", PORT);

        len = sizeof(client_addr[i]);
#ifdef _WIN32
        connected_fd[i] = accept(listening_fd, (struct sockaddr *)&client_addr[i], &len);
#endif
#ifdef __linux
        connected_fd[i] = accept(listening_fd, (struct sockaddr *)&client_addr[i], (socklen_t *)&len);
#endif

        if (connected_fd[i] < 0)
        {
            perror("*** server: accept ***");
            for (int j = 0; j < CLIENT_NUM; j++)
            {
                close(listening_fd);
                wait_clear_list(listening_fd); //FD_CLR(listening_fd, &readfds);
            }
            exit(1);
        }
        printf("fd %d\n", connected_fd[i]);
        printf("Accepted connection. %d\n", i);
        //close(listening_fd); //切断後再接続不可能？
        Setstatus(i, CONNECTED);
        wait_set_list(connected_fd[i]); // FD_SET(connected_fd[i], &readfds);
        find_unused_fd();
    }
    int close_server(int num)
    {
        bool fd_change = false;
        if (num >= CLIENT_NUM) //error
            return -1;
        printf("close\n");
        /* ソケット切断 */
        fd_change = (connected_fd[num] == maxfd);
        wait_clear_list(connected_fd[num]); //FD_CLR(connected_fd[num], &readfds);
        close(connected_fd[num]);
        Setstatus(num, LISTENING);
        return 0;
    }

    void Setstatus(int num, enum Status s)
    {
        st[num] = s;
    }
    void Setstatus(enum Status s)
    {
        for (int num = 0; num < CLIENT_NUM; num++)
        {
            Setstatus(num, s);
        }
    }
    enum Status Getstatus(int num)
    {
        return st[num];
    }
    void wait_set_list(int fd)
    {
        int i = 0;
        while (wait_set_fd[i] != 0)
            i++;

        wait_set_fd[i] = fd;
    }
    void wait_clear_list(int fd)
    {
        int i = 0;
        while (wait_clear_fd[i] != 0)
            i++;

        wait_clear_fd[i] = fd;
    }
    void list_zero()
    {
        int i = 0;
        listening_fd = 0;
        for (int i = 0; i < CLIENT_NUM; i++)
        {
            connected_fd[i] = 0;
        }
        for (int i = 0; i < CLIENT_NUM * 2; i++)
        {
            wait_set_fd[i] = 0;
            wait_clear_fd[i] = 0;
        }
    }
};
class DataSend : public Main_server_user, public Main_server
{
  public:
    DataSend(int user_cl_num, int user_port_num, int in_cl_num, int *port_num) : Main_server_user(user_cl_num, user_port_num), Main_server(in_cl_num, port_num)
    {
        buf_size_max = 1024;
        buf = new char[buf_size_max];
        tmp = new char[buf_size_max];
        io_status = new enum IO_Status[user_cl_num];
        for (int i = 0; i < user_cl_num; i++)
        {
            io_status[i] = USER_WAIT;
        }
        t_val.tv_sec = 5;
        FD_ZERO(&readfds);
        maxfd = 0;
#ifdef _WIN32
        if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0)
        {
            printf("error\n");
            exit(1);
        }
#endif
    }
    ~DataSend()
    {
        delete[] buf;
        delete[] tmp;
        delete[] io_status;
#ifdef _WIN32
        WSACleanup();
#endif
    }

  protected:
    int buflen;
    char *buf;
    char *tmp;
    int buf_size_max;
    int sel;
    fd_set fds, readfds;
    int maxfd;
    struct timeval t_val;
    int thread_commnet_num;
    int user_commnet_num;
    enum Inside_Client
    {
        LOG_WRITE,
        LOG_READ,
        THREAD_MANAGE,
        INOUT
    } in_client;
    enum IO_Status
    {
        USER_WAIT,
        IO_WAIT,
        INSIDE_WAIT
    } * io_status;
    struct IN_BUF
    {
        int user_num;
        char *inside_buf;
        // struct IN_BUF *next;
        enum Inside_Client wait_cl;
        enum Inside_Client from_cl;
        enum Inside_Client to_cl;
    };
    vector<struct IN_BUF> in_buf;
#ifdef _WIN32
    WSADATA wsaData;
#endif
  public:
    void server_func()
    {
        Listening_socket_init();
        Main_server::Listening_socket();
        wait_set_fd_set();
        Select();
    }
    void Select()
    {
        while (1)
        {
            memcpy(&fds, &readfds, sizeof(fd_set));

            if ((sel = select(maxfd + 1, &fds, NULL, NULL, &t_val)) < 0) //sel<0 error
            {
                perror("*** server: select ***");
                exit(1);
            }
            /* else if (sel == 0)
            { //time out
                puts("time out");
                break;
            }*/
            else
            {
                //printf("%d\n", maxfd);
                for (int i = 0; i < Main_server::CLIENT_NUM; i++)
                {
                    if (Main_server::Getstatus(i) == Main_server::Status::LISTENING && FD_ISSET(Main_server::listening_fd[i], &fds))
                    {
                        Main_server::connect_server(i);
                        wait_set_fd_set();
                        //FD_CLR(Main_server_user::connected_fd[i], &fds);
                        break;
                    }
                }
                for (int i = 0; i < Main_server_user::CLIENT_NUM; i++)
                {
                    if (Main_server_user::Getstatus(i) == Main_server_user::Status::LISTENING && FD_ISSET(Main_server_user::listening_fd, &fds))
                    {
                        Main_server_user::connect_server(i);
                        sprintf(buf, "user add %d,\0", i);
                        send_to_inside(INOUT);
                        wait_set_fd_set();
                        break;
                    }
                }
                for (i = 0; i < Main_server::CLIENT_NUM; i++)
                {
                    if (Main_server::Getstatus(i) == Main_server::Status::CONNECTED && FD_ISSET(Main_server::connected_fd[i], &fds))
                    {
                        recv_from_inside_struct_set(i);
                    }
                }
                inout_client();
                //data_send_user();
                //data_send();
            }
        }
    }

    void data_send()
    {
        int i;
        /* データの送受信 */
        for (i = 0; i < Main_server::CLIENT_NUM; i++)
        {
            if (Main_server::Getstatus(i) == Main_server::Status::CONNECTED && FD_ISSET(Main_server::connected_fd[i], &fds))
            {
                if ((buflen = recv(Main_server::connected_fd[i], buf, sizeof(buf), 0)) <= 0)
                {
                    printf("no recv\n");
                    Main_server::close_server(i);
                    wait_clear_fd_clear();
                    continue;
                }
                printf(">>> Received (size:%d).\n", buflen);
                write(1, buf, buflen);
                printf("<<< Sending...\n");
                if (send(Main_server::connected_fd[i], buf, buflen, 0) <= 0)
                {
                    puts("no send\n");
                    Main_server::close_server(i);
                    wait_clear_fd_clear();
                    continue;
                }
            }
        }
    }

    void clear_buf()
    {
        int i;
        for (i = 0; i < buf_size_max; i++)
        {
            buf[i] = '\0';
        }
    }
    bool recv_from_user(int i)
    {
        clear_buf();
        if ((buflen = recv(Main_server_user::connected_fd[i], buf, sizeof(char) * buf_size_max, 0)) <= 0)
        {
            printf("user%d:no recv\n", i);
            Main_server_user::close_server(i);
            sprintf(buf, "user delete %d,\0", i);
            send_to_inside(INOUT);
            wait_clear_fd_clear();
            return false;
        }
        return true;
    }
    bool send_to_user(int i)
    {
        buflen = strlen(buf) * sizeof(char);
        if (send(Main_server_user::connected_fd[i], buf, buflen, 0) <= 0)
        {
            puts("no send\n");
            Main_server_user::close_server(i);
            sprintf(buf, "user delete %d:", i);
            send_to_inside(INOUT);
            wait_clear_fd_clear();
            return false;
        }
        return true;
    }
    bool recv_from_inside(int i)
    {
        clear_buf();
        if ((buflen = recv(Main_server::connected_fd[i], buf, sizeof(char) * buf_size_max, 0)) <= 0)
        {
            printf("inside:no recv\n");
            Main_server::close_server(i);
            wait_clear_fd_clear();
            return false;
        }
        printf("inside recv:%s\n", buf);
        return true;
    }

    /*受信したbufを構造体に格納*/
    bool recv_from_inside_struct_set(int i)
    {
        struct IN_BUF tmp;
        int tmp_buflen;
        bool flag = true;
        /*bufの内容
        "w_i%d:i%d:u%d:%s\n"-> wait inside client:send client add:user num:buf 
        "w_u%d:i%d:u%d:%s\n"-> wait user:send client add:user num:buf 
        "w_i%d:u%d:%s\n"-> wait inside client:send user num:buf 
        "w_u%d:u%d:%s\n"-> wait user:send user num:buf 
        */

        clear_buf();
        if ((buflen = recv(Main_server::connected_fd[i], buf, sizeof(char) * buf_size_max, 0)) <= 0)
        {
            printf("inside:no recv\n");
            Main_server::close_server(i);
            wait_clear_fd_clear();
            return false;
        }
        printf("inside recv:%s\n", buf);

        //送信クライアント
        tmp->from_cl = i;
        switch (buf[2])
        {
        case 'i':
            strcpy(buf, buf + sizeof(char) * 3);
            buf_pick_up_int(&(tmp->wait_cl));
            if (buf[1] = 'u')
            {
                strcpy(buf, buf + sizeof(char) * 2);
                buf_pick_up_int(&(tmp->user_num));
            }
            if (buf[1] = 'i')
            {
                strcpy(buf, buf + sizeof(char) * 2);
                buf_pick_up_int(&(tmp->to_cl));

                strcpy(buf, buf + sizeof(char) * 2);
                buf_pick_up_int(&(tmp->user_num));
            }
            break;
        case 'u':
            strcpy(buf, buf + sizeof(char) * 3);
            buf_pick_up_int(&(tmp->user_num));
            int u;
            if (buf[1] = 'u')
            {
                strcpy(buf, buf + sizeof(char) * 2);
                if (tmp->user_num != buf_pick_up_int(&u))
                    cout << "user num not equal " << endl;
            }
            if (buf[1] = 'i')
            {
                strcpy(buf, buf + sizeof(char) * 2);
                buf_pick_up_int(&(tmp->to_cl));

                strcpy(buf, buf + sizeof(char) * 2);
                if (tmp->user_num != buf_pick_up_int(&u))
                    cout << "user num not equal " << endl;
            }
            break;
        default:
            cout << "error" << endl;
            flag = false;
            break;
        }
        if (flag)
            in_buf.push_back(tmp);
        return flag;
    }
    bool buf_pick_up_int(int *num)
    {
        buflen = strlen(buf);
        *num = (int)strtol(buf, &buf, 10);
        //printf("num:%d :: buf:%s\n", num, buf);

        if (buflen != strlen(buf))
        {
            printf("buf:%s\n", buf);
            return true;
        }
        else
        {
            puts("error");
            return false;
        }
    }
    bool send_to_inside(int i)
    {
        buflen = strlen(buf) * sizeof(char);
        if (send(Main_server::connected_fd[i], buf, buflen, 0) <= 0)
        {
            puts("inside:no send\n");
            Main_server::close_server(i);
            wait_clear_fd_clear();
            return false;
        }

        return true;
    }
    /*
Main_server::connected_fd[i]
i   port    name                   
0:  59631   過去ログの格納          
1:  59632   過去ログの読み込み     
2:  59633   スレッド処理          
3:  59634   ユーザーの操作処理
4:  59635
*/
    /*
diff c c++
buflen = read(fd, buf, sizeof(buf) - 1);
buflen = recv(Main_server::connected_fd[i], buf, sizeof(buf), 0))

write(fd, buf, sizeof(buf));
send(Main_server::connected_fd[i], buf, buflen, 0);
*/

    /*
機能
ユーザーから受信したコマンドをinout処理に送信
inoutが指定したクライアントにデータを送信
*/
    void inout_client()
    {
        int i, send_add[Main_server_user::CLIENT_NUM];
        bool result;

        for (i = 0; i < Main_server::CLIENT_NUM; i++)
        {

            if (Main_server_user::Getstatus(i) == Main_server_user::Status::CONNECTED)
            {
                while (1)
                {

                    result = true;
                    switch (io_status[i])
                    {
                    case USER_WAIT: //ユーザーの操作待ち

                        //if (FD_ISSET(Main_server_user::connected_fd[i], &fds))
                        //{
                        if (!recv_from_user(i))
                        {
                            result = false;
                            break;
                        }
                        else
                        {
                            strcpy(tmp, buf);
                            printf("%s\n", buf);
                            sprintf(buf, "io func:u%d:%s", i, tmp);
                            printf("%s\n", buf);
                            send_to_inside(INOUT);
                            io_status[i] = IO_WAIT;
                            //break;
                        }
//}
#ifdef pastcode
                    case IO_WAIT: //inoutの返答待ち
                        if (Main_server::Getstatus(INOUT) != Main_server::Status::CONNECTED)
                            result = false;

                        //else if (FD_ISSET(Main_server::connected_fd[INOUT], &fds))

                        if (recv_from_inside(INOUT))
                        {
                            /*
                            送信先 i+num u+num i=inside u=user
                            次状態 is_w:INSIDE_WAIT,us_w=USER_WAIT,io_w=IO_WAIT
                            */
                            int st_len = 5;
                            if (strncmp(buf, "is_w:", st_len) == 0)
                                io_status[i] = INSIDE_WAIT;
                            else if (strncmp(buf, "us_w:", st_len) == 0)
                                io_status[i] = USER_WAIT;
                            else if (strncmp(buf, "io_w:", st_len) == 0)
                                io_status[i] = IO_WAIT;

                            if (buf[st_len] == 'i')
                            {
                                send_add[i] = (int)strtol(buf + sizeof(char) * (st_len + 1), &buf, 10);
                                //buf = buf + sizeof(char);
                                strcpy(buf, buf + sizeof(char));
                                printf("buf:%s\n", buf);

                                //   send_to_inside(send_add[i]);
                            }
                            else if (buf[st_len] == 'u')
                            {
                                if (i == (int)strtol(buf + sizeof(char) * (st_len + 1), &buf, 10))
                                {
                                    strcpy(buf, buf + sizeof(char));
                                    printf("buf:%s\n", buf);
                                    send_to_user(i);
                                }
                            }
                            else
                                result = false;
                        }

                        break;
#endif
#ifdef pastcode
                    case INSIDE_WAIT: //inout以外の内部クライアントの返答待ち
                        //クライアントが存在しないためテスト用の処理
                        //sprintf(buf, "user%d:test\n", i);
                        sprintf(buf, "io func:u%d:thread_q\n", i);
                        //send_to_user(i);
                        send_to_inside(INOUT);
                        recv_from_inside(INOUT);
                        send_to_user(i);
                        //choose thread
                        recv_from_user(i);
                        strcpy(tmp, buf);
                        printf("--------%s\n", buf);
                        sprintf(buf, "io func:u%d:%s", i, tmp);
                        printf("---------%s\n", buf);
                        send_to_inside(INOUT);
                        recv_from_inside(INOUT);
                        //read thread
                        sprintf(buf, "io func:u%d:thread exist", i);
                        send_to_inside(INOUT);

                        for (int i = 0; i < 1; i++)
                        {
                            recv_from_inside(INOUT);
                            send_to_inside(INOUT);
                            recv_from_inside(INOUT);
                            send_to_user(i);
                            recv_from_user(i);
                        }
                        printf("buf:%s\n", buf);
                        //command
                        /*
                        recv_from_inside(INOUT);
                        send_to_user(i);
*/
                        io_status[i] = IO_WAIT;
                        break;
                        /*
                        if (Main_server::Getstatus(send_add[i]) != Main_server::Status::CONNECTED)
                            result = false;
                        else if (recv_from_inside(send_add[i]))
                        {
                            send_to_inside(INOUT);
                            io_status[i] = IO_WAIT;
                        }
                        break;
                        */
#endif
                    }
                    if (!result) //いずれかのクライアントでエラーが発生
                    {
                        printf("user%d:inout function failed", i);
                        io_status[i] = USER_WAIT;
                        break;
                    }
                }
            }
        }
    }
    void data_send_user()
    {
        int i;
        /* データの送受信 */
        for (i = 0; i < Main_server_user::CLIENT_NUM; i++)
        {
            if (Main_server_user::Getstatus(i) == Main_server_user::Status::CONNECTED && FD_ISSET(Main_server_user::connected_fd[i], &fds))
            {
                if ((buflen = recv(Main_server_user::connected_fd[i], buf, sizeof(buf), 0)) <= 0)
                {
                    printf("no recv\n");
                    Main_server_user::close_server(i);
                    sprintf(buf, "user delete %d,\0", i);
                    send_to_inside(INOUT);
                    wait_clear_fd_clear();
                    continue;
                }
                printf(">>> Received (size:%d).\n", buflen);
                write(1, buf, buflen);
                printf("<<< Sending...\n");
                if (send(Main_server_user::connected_fd[i], buf, buflen, 0) <= 0)
                {
                    puts("no send\n");
                    Main_server_user::close_server(i);
                    wait_clear_fd_clear();
                    continue;
                }
            }
        }
    }
    void Setmaxfd(int fd)
    {
        if (fd > maxfd)
        {
            maxfd = fd;
            printf("maxfd:%d\n", maxfd);
        }
    }
    void Setmaxfd()
    {
        int i;
        maxfd = 0;
        for (i = 0; i < Main_server::CLIENT_NUM; i++)
        {
            switch (Main_server::Getstatus(i))
            {
            case Main_server::Status::UNUSED:
                break;
            case Main_server::Status::LISTENING:
                Setmaxfd(Main_server::listening_fd[i]);
                break;
            case Main_server::Status::CONNECTED:
                Setmaxfd(Main_server::connected_fd[i]);
                break;
            }
        }

        Setmaxfd(Main_server_user::listening_fd); //常時listening中

        for (i = 0; i < Main_server_user::CLIENT_NUM; i++)
        {
            switch (Main_server_user::Getstatus(i))
            {
            case Main_server_user::Status::UNUSED:
                break;
            case Main_server_user::Status::LISTENING:
                break;
            case Main_server_user::Status::CONNECTED:
                Setmaxfd(Main_server_user::connected_fd[i]);
                break;
            }
        }
    }
    void wait_set_fd_set()
    {
        int i = 0;
        while (Main_server::wait_set_fd[i] != 0 && i < Main_server::CLIENT_NUM * 2)
        {
            FD_SET(Main_server::wait_set_fd[i], &readfds);
            Setmaxfd(Main_server::wait_set_fd[i]);
            Main_server::wait_set_fd[i] = 0;
            i++;
        }
        i = 0;
        while (Main_server_user::wait_set_fd[i] != 0 && i < Main_server_user::CLIENT_NUM + 1)
        {
            FD_SET(Main_server_user::wait_set_fd[i], &readfds);
            Setmaxfd(Main_server_user::wait_set_fd[i]);
            Main_server_user::wait_set_fd[i] = 0;
            i++;
        }
    }
    void wait_clear_fd_clear()
    {
        int i = 0;
        while (Main_server::wait_clear_fd[i] != 0 && i < Main_server::CLIENT_NUM * 2)
        {
            FD_CLR(Main_server::wait_clear_fd[i], &readfds);
            if (maxfd == Main_server::wait_clear_fd[i])
                Setmaxfd();
            Main_server::wait_clear_fd[i] = 0;
            i++;
        }
        i = 0;
        while (Main_server_user::wait_clear_fd[i] != 0 && i < Main_server_user::CLIENT_NUM + 1)
        {
            FD_CLR(Main_server_user::wait_clear_fd[i], &readfds);
            if (maxfd == Main_server_user::wait_clear_fd[i])
                Setmaxfd();
            Main_server_user::wait_clear_fd[i] = 0;
            i++;
        }
    }
};

int main(void)
{
    int client_num, *port_number_list, i;
    client_num = 5;
    port_number_list = new int[client_num];
    for (i = 0; i < client_num; i++)
        port_number_list[i] = 59631 + i;

    //Main_server server(client_num, port_number_list);
    //引数 外部のクライアント数,外部の待ちポート番号,内部のクライアント数,内部の待ちポート番号リスト
    DataSend server(client_num, 59630, client_num, port_number_list);

    printf("Waiting for connections from a client.\n");
    server.server_func();
    delete[] port_number_list;
    return 0;
}
