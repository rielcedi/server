/*
 * client.cpp
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
using namespace std;

#ifdef __linux
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#ifdef _WIN32
#include <winsock2.h>
/*
compile 
g++ -lwsock32 inout_client.cpp -lws2_32 -o io_client
*/
#endif

class Client
{
  public:
    Client(int port_num, char *main_server_address)
    {
        PORT = port_num;
        ipaddr = main_server_address;
        fd = 0;
        buf_size_max = 1024;
        buf = new char[buf_size_max];
#ifdef _WIN32
        WSAStartup(MAKEWORD(2, 0), &wsaData);
#endif
    }
    ~Client()
    {
        PORT = 0;
        delete[] buf;
#ifdef _WIN32
        WSACleanup();
#endif
    }

  protected:
    struct sockaddr_in addr;
    int fd;
    int buflen;
    char *buf;
    int buf_size_max;
    int PORT;
    int u_num;
    char *ipaddr;
    enum Inside_Client
    {
        LOG_WRITE,
        LOG_READ,
        THREAD_MANAGE,
        INOUT
    } in_client;
    enum User_status
    {
        UNSET,
        READ_TH_LIST,
        CHOOSE_TH,
        READ_TH,
        READ_COMMENT,
        WAIT_COMA,
        COMMAND,
        ADD_TH,
        COMMNET,
        UPDATE
    };
    vector<int> exist_user;
    vector<int> user_comment_num; //ユーザーごとの最新コメント番号
    vector<enum User_status> user_status;
#ifdef _WIN32
    WSADATA wsaData;
#endif
  public:
    void clinet_function()
    {

        make_socket();
        client_connect();
        while (1)
        {
            /*接続時の初期設定
        最大ユーザー数
        存在するユーザーの番号
        各ユーザーの最新コメント番号
        buf="user1 num,...,user_num"
        */
            if (recv_main_server())
                write(1, buf, strlen(buf));
            if (strncmp(buf, "user add", 8) == 0)
            {
                puts("user add");
                strcpy(buf, buf + sizeof(char) * strlen("user add"));
                add_user();
            }
            else if (strncmp(buf, "user delete", 11) == 0)
            {
                puts("delte user");
                delete_user();
            }
            else if (strncmp(buf, "io func", 7) == 0)
            {
                puts("io func");
                strcpy(buf, buf + sizeof(char) * strlen("io func:u"));
                if (io_user_number())
                    inout_function_user();
                else
                    cout << "this user dont exist" << endl;
            }
            else
                puts("recv error");
        }
    }
    void make_socket()
    {
        /* ソケット作成 */
        if ((fd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("*** client: socket ***");
            exit(1);
        }
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = PF_INET;
        addr.sin_addr.s_addr = inet_addr(ipaddr);
        addr.sin_port = htons(PORT);
    }

    void client_connect()
    {
        /* ソケット接続要求 */
        if (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
            perror("*** client: connect ***");
            exit(close(fd));
            exit(1);
        }
        printf("Connected to server.\n");
        //  printf("Input 'quit' to disconnect...\n");
    }
    bool recv_main_server()
    {
        if ((buflen = recv(fd, buf, sizeof(char) * buf_size_max, 0)) <= 0)
        {
            printf("no recv\n");
            exit(close(fd));
            return false;
        }
        buf[buflen] = '\0';
        printf("recv buf:%s\n", buf);
        return true;
    }
    bool send_main_server()
    {
        buflen = strlen(buf) * sizeof(char);
        printf("buf send:%s\n", buf);
        if (send(fd, buf, buflen, 0) <= 0)
        {
            puts("no send\n");
            exit(close(fd));
            return false;
        }
        return true;
    }
    void add_user()
    {
        int j, u_num;
        bool flag;
        /*
        for (j = 0, u_num = 0, flag = false; buf[j] != '\0'; j++)
        {
            //printf("buf:%c\n", buf[j]);
            if ('0' <= buf[j] && buf[j] <= '9')
            {
                u_num = u_num * 10 + (int)(buf[j] - '0');
                flag = true;
            }
            else
            {
                if (flag && (exist_user.empty() || (find(exist_user.begin(), exist_user.end(), u_num) == exist_user.end())))
                {
                    exist_user.push_back(u_num);
                    std::cout << "exit_user " << exist_user.back() << "\n";
                    user_comment_num.push_back(0);
                    user_status.push_back(UNSET);
                    printf("u_num:%d\n", u_num);
                }
                */
        buflen = strlen(buf);
        u_num = (int)strtol(buf, &buf, 10);
        if (strlen(buf) != buflen)
        {
            strcpy(buf, buf + sizeof(char));
            printf("u_num:%d :: buf:%s\n", u_num, buf);

            if ((exist_user.empty() || (find(exist_user.begin(), exist_user.end(), u_num) == exist_user.end())))
            {
                exist_user.push_back(u_num);
                std::cout << "exit_user " << exist_user.back() << "\n";
                user_comment_num.push_back(0);
                user_status.push_back(UNSET);
                printf("u_num:%d\n", u_num);
            }
        }
        u_num = 0;
        //flag = false;
        // }
        //}
        //sort(exist_user.begin(), exist_user.end());
    }
    void delete_user()
    {
        int j, u_num;
        bool flag;
        vector<int>::iterator itr;
        for (j = 0, u_num = 0, flag = false; buf[j] != '\0'; j++)
        {
            // printf("buf:%c\n", buf[j]);
            if ('0' <= buf[j] && buf[j] <= '9')
            {
                u_num = u_num * 10 + (int)(buf[j] - '0');
                flag = true;
            }
            else
            {
                if (flag && ((itr = find(exist_user.begin(), exist_user.end(), u_num)) != exist_user.end()))
                {
                    exist_user.erase(itr);
                    //cout << "exit_user " << exist_user.end() - exist_user.begin() << "\n";
                    //cout << "exit_user " << exist_user.back() << "\n";
                    user_comment_num.erase(user_comment_num.begin() + (itr - exist_user.begin()));
                    user_status.erase(user_status.begin() + (itr - exist_user.begin()));
                    cout << itr - exist_user.begin() << endl;
                    printf("user size:%d\n", exist_user.size());
                    printf("commnet size:%d\n", user_comment_num.size());
                }
                u_num = 0;
                flag = false;
            }
        }
    }
    bool buf_pick_up_int(int *num)
    {
        buflen = strlen(buf);
        *num = (int)strtol(buf, &buf, 10);
        //printf("num:%d :: buf:%s\n", num, buf);

        if (buflen != strlen(buf))
        {
            printf("buf:%s\n", buf);
            return true;
        }
        else
        {
            puts("error");
            return false;
        }
    }
    /*内部クライアントに送信するbuf用*/
    void buf_setting(bool waiting_inside, int wait_client, int uadd, int inputadd)
    {
        string str(buf);
        if (waiting_inside)
            sprintf(buf, "w_i%d:i%d:u%d:%s\n", wait_client, uadd, inputadd, str.c_str());
        else
            sprintf(buf, "w_u%d:i%d:u%d:%s\n", wait_client, uadd, inputadd, str.c_str());
        printf("%s\n", buf);
    }
    /*ユーザーに送信するbuf用*/
    void buf_setting(bool waiting_inside, int wait_client, int uadd)
    {
        string str(buf);
        if (waiting_inside)
            sprintf(buf, "w_i%d:u%d:%s\n", wait_client, uadd, str.c_str());
        else
            sprintf(buf, "w_u%d:u%d:%s\n", wait_client, uadd, str.c_str());
        printf("%s\n", buf);
    }
    /*
diff c c++
buflen = read(fd, buf, sizeof(buf) - 1);
buflen = recv(Main_server::connected_fd[i], buf, sizeof(buf), 0))

write(fd, buf, sizeof(buf));
send(Main_server::connected_fd[i], buf, buflen, 0);
*/

    bool io_user_number()
    {
        int new_buflen;
        buflen = strlen(buf);
        u_num = (int)strtol(buf, &buf, 10);
        printf("u_num:%d\nbuf->%s\n", u_num, buf);

        if (buflen != (new_buflen = strlen(buf)))
        {
            for (int j = 0; buf[j] != '\0'; j++)
            {
                buf[j] = buf[j + 1];
            }
            //buf[i]='\0';
            printf("buf->%s\n", buf);
            return true;
        }
        else
        {
            puts("error");
            return false;
        }
    }
    bool inout_function_user()
    {
        //bufにはサーバーから送られてきたコマンドが格納されている。
        //keijiban:掲示板の過去ログを表示
        //comment:入力されたコメントを過去ログに格納
        //update:最新の過去ログをユーザー側に表示
        //quit:掲示板の通信を終了

        int j, update_comment_num;
        bool flag;
        string str;
        vector<int>::iterator itr;
        int index;
        /***コマンド受付***/
        if ((itr = find(exist_user.begin(), exist_user.end(), u_num)) != exist_user.end())
        {
            index = (itr - exist_user.begin());
            printf("index:%d\nstatus:%d\n", index, user_status[index]);
            switch (user_status[index])
            {
                /***keijibanコマンドが入力されたとき***/
            case UNSET:
                str = buf;
                if (str.find("keijiban") != string::npos)
                {
                    user_status[index] = READ_TH_LIST;
                    //ユーザーにスレッドを選択させたい。
                    //スレッド管理プログラムを呼んでスレッド一覧を表示させる。
                    //sprintf(buf, "is_w:i2:thread");
                    strcpy(buf, "thread list");
                    buf_setting(true, THREAD_MANAGE, u_num, THREAD_MANAGE);
                    /*
                    エラー時の処理は未実装
                    スレッド管理との通信が確立されていないとき
                    スレッド管理との通信が途切れたとき
                    */
                    send_main_server();
                }
                else
                    cout << "invail command" << endl;
                break;
            case READ_TH_LIST:
                /***スレッド管理プログラムからスレッド一覧を表示する。***/
                //スレッド管理ファイルからスレッドをbufで受け取る。
                //recv_main_server();
                //ユーザーにbuf(スレッド名が格納されている)を表示する。
                //str.assign(buf);
                str = buf;
                if (str.find("thread_q") == string::npos)
                {
                    printf("u%d:thread list \n", u_num);
                    //sprintf(buf, "is_w:u%d:%s", u_num, str.c_str());
                    buf_setting(true, THREAD_MANAGE, u_num);

                    send_main_server();
                }
                else
                {
                    //スレッドを表示し終えたのでスレッドの表示を終了する。
                    /***ユーザーにスレッドを選ばせる。***/
                    printf("u%d:thread list end\n", u_num);
                    user_status[index] = CHOOSE_TH;
                    //sprintf(buf, "us_w:u%d:please choose thread.\n", u_num);
                    strcpy(buf, "please choose thread.\n");
                    buf_setting(false, u_num, u_num);
                    send_main_server();
                    //sprintf(buf, "us_w:i2:thread");
                    //send_main_server();
                }
                break;
            case CHOOSE_TH:
                /***ユーザーが選んだスレッドを受け取る。***/
                //recv_main_server();
                user_status[index] = READ_TH;
                //選ばれたスレッドを読み込みプログラムに送る。
                //str = "is_w:i1:" + str;
                //strcpy(buf, str.c_str());
                buf_setting(true, LOG_READ, u_num);
                send_main_server();
                update_comment_num = 50;
                user_comment_num[index] = 123456; //debug
            case READ_TH:
                //str = buf;
                //if (str.find("thread exist") != string::npos)
                if (strcmp("thread exist", buf) != 0)
                {
                    cout << "thread is not exist" << endl;
                    user_status[index] = CHOOSE_TH;
                    break;
                }
                //break;
            case READ_COMMENT:
                //読み込みプログラムに最新50個を出力するように伝える。
                //最新コメント番号を取得
                //recv_main_server();
                //str = buf;
                //bufに読み込んでほしいコメントの番号(つまりi)を格納する。
                for (int i = 0; i < update_comment_num; i++)
                {
                    //sprintf(buf, "is_w:i1:%d", user_comment_num[index]);
                    buf_setting(true, LOG_READ, u_num);
                    user_comment_num[index]++;
                    //読み込みプログラムに読み込んでほしいコメントの番号を送る。
                    send_main_server();
                    /***読み込みプログラムからコメントの文字列を受け取る。***/
                    recv_main_server();
                    //受け取った文字列bufをユーザー側に送る。(表示する)
                    str = buf;
                    //sprintf(buf, "is_w:u%d:%s", u_num, str.c_str());
                    buf_setting(false, u_num, u_num);
                    send_main_server();
                }
                user_status[index] = WAIT_COMA;
                // break;
            /***掲示板の使い方(コマンドの種類)をユーザーに伝える。***/
            case WAIT_COMA:
                cout << "sndipwnifwp" << endl;
                //sprintf(buf, "us_w:u%d:please choose command in [add_thread/comment/update/quit]\n", u_num);
                strcpy(buf, "please choose command in [add_thread/comment/update/quit]\n");
                buf_setting(false, u_num, u_num);
                send_main_server();
                user_status[index] = COMMAND;
                break;
            case COMMAND:
                //keijibanの処理を行う。
                //comment:update:quitどれかのコマンドが送られてくる。
                //ユーザーから送られてきたコマンドをbufに格納する。
                str = buf;
                cout << buf << endl
                     << "command" << endl;

                if (str.find("add_thread") != string::npos)
                //if (strncmp(buf, "add_thread", strlen("add_thread")) == 0)
                {
                    //新しいスレッドの名前をユーザーに書かせるように説明を送る。（日本語禁止）
                    // sprintf(buf, "us_w:u%d:please write new thread name.\n", u_num);
                    strcpy(buf, "please write new thread name.\n");
                    buf_setting(false, u_num, u_num);
                    send_main_server();
                    user_status[index] = ADD_TH;
                }
                else if (str.find("comment") != string::npos)
                //   else if (strncmp(buf, "comment", strlen("comment")) == 0)
                {
                    //commentコマンドが入力された場合
                    //以下にコメントを書いてくださいと伝える。
                    //sprintf(buf, "us_w:u%d:please write your comment. \n", u_num);
                    strcpy(buf, "please write your comment. \n");
                    buf_setting(false, u_num, u_num);
                    send_main_server();
                    user_status[index] = COMMNET;
                }
                else if (str.find("update") != string::npos)
                // else if (strncmp(buf, "update", strlen("update")) == 0)
                {
                    //sprintf(buf, "us_w:u%d:how many update comment?\n", u_num);
                    strcpy(buf, "how many update comment?\n");
                    buf_setting(false, u_num, u_num);
                    send_main_server();
                    user_status[index] = UPDATE;
                }
                else
                {
                    cout << "invaild command" << endl;
                    //sprintf(buf, "io_w:u%d:invaild command\n", u_num);
                    strcpy(buf, "invaild command\n");
                    buf_setting(true, INOUT, u_num);
                    send_main_server();
                    user_status[index] = WAIT_COMA;
                }

                break;

            case ADD_TH:
                /***ユーザーが書いた新しいスレッド名を受け取る。***/
                //このスレッド名をスレッド管理プログラムに送る。
                //str = buf;
                //sprintf(buf, "is_w:i2:%s", str.c_str());
                buf_setting(true, THREAD_MANAGE, u_num);
                user_status[index] = WAIT_COMA;
                send_main_server();
                break;
            case COMMNET:
                /***ユーザーが書き込んだ内容をbufに格納する。***/
                //過去ログに格納するプログラムを呼び出して、bufを渡す。
                //str = buf;
                //sprintf(buf, "is_w:i0:%s", str.c_str());
                buf_setting(true, LOG_WRITE, u_num);
                send_main_server();
                recv_main_server();
                //格納プログラムから書き込んだとの確認信号を受けてこの処理は終了する。
                if (strcmp(buf, "ok") == 0)
                    user_status[index] = WAIT_COMA;
                else
                {
                    //sprintf(buf, "io_w:u%d:ERROR:cannot write in the past log.\n", u_num);
                    strcpy(buf, "ERROR:cannot write in the past log.\n");
                    buf_setting(true, INOUT, u_num);
                    send_main_server();
                    user_status[index] = WAIT_COMA;
                }
                break;
            case UPDATE:
                //updateを要求したユーザーの最新番号を取得する。
                //user_comment_num[index]
                //読み込むコメント数を取得
                if (buf_pick_up_int(&(user_comment_num[index])))
                {
                    /***過去ログの最新番号を取得***/
                    //strcpy(buf, "is_w:i2:latest number\n");
                    strcpy(buf, "latest number\n");
                    buf_setting(true, LOG_READ, u_num);
                    send_main_server();
                    recv_main_server();
                    if (buf_pick_up_int(&update_comment_num))
                    {
                        if ((update_comment_num -= user_comment_num[index]) > 0)
                        {
                            user_status[index] = READ_TH;
                            break;
                        }
                    }
                }
                puts("error");
                user_status[index] = WAIT_COMA;
#ifdef debug
                //bufに読み込んでほしいコメントの番号(つまりj)を格納する。
                //sprintf(buf, "is_wi1:%d", user_comment_num[index]++);
                sprintf(buf,"%d",user_comment_num[index]++));
                buf_setting("is_w", u_num, LOG_READ);
                //読み込みプログラムに読み込んでほしいコメントの番号を送る。
                send_main_server();
                /***読み込みプログラムからコメントの文字列を受け取る。***/
                recv_main_server();

                //受け取った文字列bufをユーザー側に送る。(表示する)
                //str = buf;
                //sprintf(buf, "is_w:u%d:%s", u_num, str.c_str());
                buf_setting("is_w", u_num);
                send_main_server();
                user_status[index] = WAIT_COMA;
#endif
                break;
            }
        }
        return true;
    }
};

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s <IP Address>\n", argv[0]);
        exit(1);
    }

    Client client(59634, argv[1]);
    client.clinet_function();

    return 0;
}

#ifdef debug
void inout_function_user()
{
    //bufにはサーバーから送られてきたコマンドが格納されている。
    //keijiban:掲示板の過去ログを表示
    //comment:入力されたコメントを過去ログに格納
    //update:最新の過去ログをユーザー側に表示
    //quit:掲示板の通信を終了
    int j, u_num;
    bool flag;

    for (j = 0, u_num = 0, flag = false; buf[j] != '\0'; j++)
    {
        printf("buf:%c\n", buf[j]);
        if ('0' <= buf[j] && buf[j] <= '9')
        {
            u_num = u_num * 10 + (int)(buf[j] - '0');
            flag = true;
        }
        else if (flag)
        {
            break;
        }
        else
        {
            //ユーザー番号取得失敗
            exit(1);
        }
    }

    int i;
    for (i = 0; i < Main_server_user::CLIENT_NUM; i++)
    {

        /***コマンド受付***/
        if (!recv_from_user(i))
            continue;
        /***keijibanコマンドが入力されたとき***/
        if (strcmp(buf, "keijiban\n") == 0)
        {
            //ユーザーにスレッドを選択させたい。
            //スレッド管理プログラムを呼んでスレッド一覧を表示させる。
            strcpy(buf, "thread");
            //buf[strlen(buf)]='\0';
            /*
                    エラー時の処理は未実装
                    スレッド管理との通信が確立されていないとき
                    スレッド管理との通信が途切れたとき
                    */
            send_to_inside(THREAD_MANAGE);
        }
        /***スレッド管理プログラムからスレッド一覧を表示する。***/
        /*while (1)
                {*/
        //スレッド管理ファイルからスレッドをbufで受け取る。
        recv_from_inside(THREAD_MANAGE);
        //ユーザーにbuf(スレッド名が格納されている)を表示する。
        if (strcmp(buf, "thread_q\n") == 0)
        {
            //break; //スレッドを表示し終えたのでスレッドの表示を終了する。
        }
        if (!send_to_user(i))
            continue;
        // }
        /***ユーザーにスレッドを選ばせる。***/
        strcpy(buf, "please choose thread.\n");
        //buf[strlen(buf)] = '\0';
        if (!send_to_user(i))
            continue;

        /***ユーザーが選んだスレッドを受け取る。***/
        if (!recv_from_user(i))
            continue;
        //buf[buflen] = '\0';
        //選ばれたスレッドを読み込みプログラムに送る。
        send_to_inside(LOG_READ);
        thread_commnet_num = 0; //最新のコメント番号を取得できていない
        //読み込みプログラムに最新50個を出力するように伝える。
        //bufに読み込んでほしいコメントの番号(つまりi)を格納する。
        user_commnet_num++;
        /*if (commnet_num > 50)
                {
                    continue;
                }*/
        snprintf(buf, buf_size_max, "%s", user_commnet_num);
        //読み込みプログラムに読み込んでほしいコメントの番号を送る。
        send_to_inside(LOG_READ);
        /***読み込みプログラムからコメントの文字列を受け取る。***/
        recv_from_inside(LOG_READ);
        //buf[buflen] = '\0';
        //受け取った文字列bufをユーザー側に送る。(表示する)
        if (!send_to_user(i))
            continue;
        /***掲示板の使い方(コマンドの種類)をユーザーに伝える。***/
        strcpy(buf, "please choose command in [add_thread/comment/update/quit]\n");
        //buf[strlen(buf)] = '\0';
        if (!send_to_user(i))
            continue;

        //keijibanの処理を行う。
        //comment:update:quitどれかのコマンドが送られてくる。
        //ユーザーから送られてきたコマンドをbufに格納する。
        if (!recv_from_user(i))
            continue;
        //buf[buflen] = '\0';

        //add_threadコマンドが入力された場合
        if (strcmp(buf, "add_thread\n") == 0)
        {

            //新しいスレッドの名前をユーザーに書かせるように説明を送る。（日本語禁止）
            strcpy(buf, "please write new thread name.\n");
            //buf[strlen(buf)] = '\0';
            if (!send_to_user(i))
                continue;
        }
        /***ユーザーが書いた新しいスレッド名を受け取る。***/
        if (!recv_from_user(i))
            continue;
        //buf[buflen] = '\0';

        //このスレッド名をスレッド管理プログラムに送る。
        send_to_inside(THREAD_MANAGE);

        //commentコマンドが入力された場合
        if (strcmp(buf, "comment\n") == 0)
        {
            //以下にコメントを書いてくださいと伝える。
            strcpy(buf, "↓ please write your comment. ↓\n");
            //buf[strlen(buf)] = '\0';
            if (!send_to_user(i))
                continue;
        }
        /***ユーザーが書き込んだ内容をbufに格納する。***/
        if (!recv_from_user(i))
            continue;
        //buf[buflen] = '\0';
        //過去ログに格納するプログラムを呼び出して、bufを渡す。
        send_to_inside(LOG_WRITE);
        //格納プログラムから書き込んだとの確認信号を受けてこの処理は終了する。
        if (strcmp(buf, "ok") == 0)
        {
            continue;
        }
        else
        {
            //過去ログへの書き込みを失敗したのでユーザー側にエラーを返す。
            strcpy(buf, "ERROR:cannot write in the past log.\n");
            //buf[strlen(buf)] = '\0';
            if (!send_to_user(i))
                continue;
        }

        //updateコマンドが入力された場合
        if (strcmp(buf, "update\n") == 0)
        {
            //updateを要求したユーザーの最新番号を取得する。
            user_commnet_num = 0;
        }
        /***過去ログの最新番号を取得***/
        for (int j = user_commnet_num /*そのユーザーの最新番号*/; j <= thread_commnet_num /*過去ログの最新番号*/; j++)
        {
            //bufに読み込んでほしいコメントの番号(つまりj)を格納する。
            snprintf(buf, buf_size_max, "%s", j);
            //読み込みプログラムに読み込んでほしいコメントの番号を送る。
            send_to_inside(LOG_READ);
            /***読み込みプログラムからコメントの文字列を受け取る。***/
            recv_from_inside(LOG_READ);

            //受け取った文字列bufをユーザー側に送る。(表示する)
            if (!send_to_user(i))
                continue;
        }
    }
}
#endif
