/*
 * main.cpp
 * 
 * 未
 * タイムアウトさせるか？
 * 接続時にlistening_fdをcloseすべきではないのか？
 * ↑一度closeされたものは再度listenしても正しく接続できない
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef __linux
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#ifdef _WIN32
#include <winsock2.h>
/*
compile 
g++ -lwsock32 main.cpp -lws2_32 -o server.exe
*/
#endif

class Main_server
{
  public:
    Main_server(int cl_num, int *port_num)
    {
        CLIENT_NUM = cl_num;
        PORT = port_num;

        server_addr = new struct sockaddr_in[CLIENT_NUM];
        client_addr = new struct sockaddr_in[CLIENT_NUM];
        connected_fd = new int[CLIENT_NUM];
        listening_fd = new int[CLIENT_NUM];
        st = new enum Status[CLIENT_NUM];
        FD_ZERO(&readfds);
        Setstatus(UNUSED);
        Setmaxfd();
        t_val.tv_sec = 5; //time out second
#ifdef _WIN32
        if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0)
        {
            printf("error\n");
            exit(1);
        }
#endif
    }
    ~Main_server()
    {
        Setstatus(UNUSED);
        Setmaxfd();
        //delete[] PORT;
        delete[] server_addr;
        delete[] client_addr;
        delete[] connected_fd;
        delete[] listening_fd;
        delete[] st;
#ifdef _WIN32
        WSACleanup();
#endif
    }

  protected:
    struct timeval t_val;
    int maxfd;
    int *PORT; /* ポート番号 */
    struct sockaddr_in *server_addr;
    struct sockaddr_in *client_addr;
    int len;
    int CLIENT_NUM;
    int *connected_fd, *listening_fd;
    enum Status
    {
        UNUSED,    //未使用
        LISTENING, //接続待ち
        CONNECTED  //接続中
    } * st;
    fd_set fds, readfds;
#ifdef _WIN32
    WSADATA wsaData;
#endif
  public:
    void make_socket(int num)
    {
        /* リスニングソケット作成 */
        if ((listening_fd[num] = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("*** server: socket ***");
            exit(1);
        }

#ifdef __linux
        int reuse = 1;
        setsockopt(*listening_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int));
#endif
#ifdef _WIN32
        int reuse = 1;
        setsockopt(listening_fd[num], SOL_SOCKET, SO_REUSEADDR, (const char *)&reuse, sizeof(int));
#endif

        /* アドレスファミリ・ポート番号・IPアドレス設定 */
        memset(&server_addr[num], 0, sizeof(server_addr[num]));
        server_addr[num].sin_family = PF_INET;
        server_addr[num].sin_addr.s_addr = htonl(INADDR_ANY);
        server_addr[num].sin_port = htons(PORT[num]);

        /* リスニングソケットにアドレスを割り当て */

        if (bind(listening_fd[num], (struct sockaddr *)&server_addr[num], sizeof(server_addr[num])) < 0)
        {
            perror("*** server: bind ***");
            close(listening_fd[num]);
            exit(1);
        }
    }
    void Listening_socket(void)
    {
        int i;

        for (i = 0; i < CLIENT_NUM; i++)
        {
            if (Getstatus(i) == UNUSED)
            {
                make_socket(i);
                FD_SET(listening_fd[i], &readfds);
                Setstatus(i, LISTENING);
            }
        }
        /* リスニングソケット待ち受け */
        for (i = 0; i < CLIENT_NUM; i++)
        {
            if (Getstatus(i) == LISTENING)
            {
                if (listen(listening_fd[i], 1) < 0)
                {
                    perror("*** server: listen ***");
                    close(listening_fd[i]);
                    exit(1);
                }
                Setmaxfd(listening_fd[i]);
            }
        }
    }
    void Listening_socket(int num)
    {

        if (Getstatus(num) == UNUSED)
        {
            make_socket(num);
            FD_SET(listening_fd[num], &readfds);
            Setstatus(num, LISTENING);
        }

        /* リスニングソケット待ち受け */
        if (Getstatus(num) == LISTENING)
        {
            if (listen(listening_fd[num], 1) < 0)
            {
                perror("*** server: listen ***");
                close(listening_fd[num]);
                exit(1);
            }
            Setmaxfd(listening_fd[num]);
        }
    }
    void connect_server()
    {
        int i;
        for (i = 0; i <= CLIENT_NUM; i++)
        {
            if (Getstatus(i) == LISTENING && FD_ISSET(listening_fd[i], &fds))
            {

                /* 接続要求受け付け */
                printf("num=%d\n", PORT[i]);

                len = sizeof(client_addr[i]);
#ifdef _WIN32
                connected_fd[i] = accept(listening_fd[i], (struct sockaddr *)&client_addr[i], &len);
#endif
#ifdef __linux
                connected_fd[i] = accept(listening_fd[i], (struct sockaddr *)&client_addr[i], (socklen_t *)&len);
#endif

                if (connected_fd[i] < 0)
                {
                    perror("*** server: accept ***");
                    for (int j = 0; j < CLIENT_NUM; j++)
                    {
                        close(listening_fd[i]);
                        FD_CLR(listening_fd[i], &readfds);
                    }
                    exit(1);
                }

                //close(listening_fd[i]); //切断後再接続不可能？
                Setstatus(i, CONNECTED);
                FD_CLR(listening_fd[i], &readfds);
                FD_SET(connected_fd[i], &readfds);
                printf("Accepted connection.\n");
                Setmaxfd(connected_fd[i]);
                if (maxfd == listening_fd[i])
                {
                    Setmaxfd();
                }
            }
        }
    }
    int close_server(int num)
    {
        bool fd_change = false;
        if (num >= CLIENT_NUM) //error
            return -1;
        printf("close\n");
        /* ソケット切断 */
        fd_change = (connected_fd[num] == maxfd);
        FD_CLR(connected_fd[num], &readfds);
        close(connected_fd[num]);
        Setstatus(num, LISTENING);
        FD_SET(listening_fd[num], &readfds);
        //Listening_socket(num);
        if (fd_change)
        {
            Setmaxfd();
        }
        return 0;
    }

    void Setmaxfd(int fd)
    {
        if (fd > maxfd)
            maxfd = fd;
    }
    void Setmaxfd()
    {
        maxfd = 0;
        for (int n = 0; n < CLIENT_NUM; n++)
        {
            switch (Getstatus(n))
            {
            case UNUSED:
                break;
            case LISTENING:
                Setmaxfd(listening_fd[n]);
                break;
            case CONNECTED:
                Setmaxfd(connected_fd[n]);
                break;
            };
        }
    }
    void Setstatus(int num, enum Status s)
    {
        st[num] = s;
    }
    void Setstatus(enum Status s)
    {
        for (int num = 0; num < CLIENT_NUM; num++)
        {
            Setstatus(num, s);
        }
    }
    enum Status Getstatus(int num)
    {
        return st[num];
    }
};
class Main_server_user
{
  public:
    Main_server_user(int cl_num, int port_num)
    {
        CLIENT_NUM = cl_num;
        PORT = port_num;

        //server_addr = new struct sockaddr_in[CLIENT_NUM];
        client_addr = new struct sockaddr_in[CLIENT_NUM];
        connected_fd = new int[CLIENT_NUM];
        //listening_fd = new int[CLIENT_NUM];
        st = new enum Status[CLIENT_NUM];
        FD_ZERO(&readfds);
        Setstatus(UNUSED);
        Setmaxfd();
        t_val.tv_sec = 5; //time out second
#ifdef _WIN32
        if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0)
        {
            printf("error\n");
            exit(1);
        }
#endif
    }
    ~Main_server_user()
    {
        Setstatus(UNUSED);
        Setmaxfd();
        //delete[] server_addr;
        delete[] client_addr;
        delete[] connected_fd;
        //delete[] listening_fd;
        delete[] st;
#ifdef _WIN32
        WSACleanup();
#endif
    }

  protected:
    struct timeval t_val;
    int maxfd;
    int PORT; /* ポート番号 */
    struct sockaddr_in server_addr;
    struct sockaddr_in *client_addr;
    int len;
    int CLIENT_NUM;
    int *connected_fd, listening_fd;
    enum Status
    {
        UNUSED,    //未使用
        LISTENING, //接続待ち
        CONNECTED  //接続中
    } * st;
    fd_set fds, readfds;
#ifdef _WIN32
    WSADATA wsaData;
#endif
  public:
    void make_socket()
    {
        /* リスニングソケット作成 */
        if ((listening_fd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("*** server: socket ***");
            exit(1);
        }

#ifdef __linux
        int reuse = 1;
        setsockopt(listening_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int));
#endif
#ifdef _WIN32
        int reuse = 1;
        setsockopt(listening_fd, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuse, sizeof(int));
#endif

        /* アドレスファミリ・ポート番号・IPアドレス設定 */
        memset(&server_addr, 0, sizeof(server_addr));
        server_addr.sin_family = PF_INET;
        server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        server_addr.sin_port = htons(PORT);

        /* リスニングソケットにアドレスを割り当て */

        if (bind(listening_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
        {
            perror("*** server: bind ***");
            close(listening_fd);
            exit(1);
        }
    }
    void Listening_socket_init(void)
    {
        make_socket();
        FD_SET(listening_fd, &readfds);
        Listening_socket();
    }
    void Listening_socket(void)
    {
        int i;

        for (i = 0; i < CLIENT_NUM; i++)
        {
            if (Getstatus(i) == UNUSED)
            {
                printf("%d\n", i);
                //make_socket(i);
                //FD_SET(listening_fd, &readfds);
                Setstatus(i, LISTENING);
                if (listen(listening_fd, 1) < 0)
                {
                    perror("*** server: listen ***");
                    close(listening_fd);
                    exit(1);
                }
                Setmaxfd(listening_fd);
                break;
            }
        }
    }
    void Listening_socket(int num)
    {

        if (Getstatus(num) == UNUSED)
        {
            make_socket();
            FD_SET(listening_fd, &readfds);
            Setstatus(num, LISTENING);
        }

        /* リスニングソケット待ち受け */
        if (Getstatus(num) == LISTENING)
        {
            if (listen(listening_fd, 1) < 0)
            {
                perror("*** server: listen ***");
                close(listening_fd);
                exit(1);
            }
            Setmaxfd(listening_fd);
        }
    }
    void connect_server()
    {
        int i;
        for (i = 0; i <= CLIENT_NUM; i++)
        {
            if (Getstatus(i) == LISTENING && FD_ISSET(listening_fd, &fds))
            {

                /* 接続要求受け付け */
                printf("num=%d\n", PORT);

                len = sizeof(client_addr[i]);
#ifdef _WIN32
                connected_fd[i] = accept(listening_fd, (struct sockaddr *)&client_addr[i], &len);
#endif
#ifdef __linux
                connected_fd[i] = accept(listening_fd, (struct sockaddr *)&client_addr[i], (socklen_t *)&len);
#endif

                if (connected_fd[i] < 0)
                {
                    perror("*** server: accept ***");
                    for (int j = 0; j < CLIENT_NUM; j++)
                    {
                        close(listening_fd);
                        FD_CLR(listening_fd, &readfds);
                    }
                    exit(1);
                }
                printf("fd %d\n", connected_fd[i]);
                printf("Accepted connection. %d\n", i);
                //close(listening_fd); //切断後再接続不可能？
                Setstatus(i, CONNECTED);
                //FD_CLR(listening_fd, &readfds);
                FD_SET(connected_fd[i], &readfds);
                /*
                Setmaxfd(connected_fd[i]);
                if (maxfd == listening_fd)
                {
                    Setmaxfd();
                }
 */

                Setmaxfd();
                Setstatus(i + 1, LISTENING);
                break;
            }
        }
    }
    int close_server(int num)
    {
        bool fd_change = false;
        if (num >= CLIENT_NUM) //error
            return -1;
        printf("close\n");
        /* ソケット切断 */
        fd_change = (connected_fd[num] == maxfd);
        FD_CLR(connected_fd[num], &readfds);
        close(connected_fd[num]);
        Setstatus(num, LISTENING);
        //FD_SET(listening_fd, &readfds);
        //Listening_socket(num);
        if (fd_change)
        {
            Setmaxfd();
        }
        return 0;
    }

    void Setmaxfd(int fd)
    {
        if (fd > maxfd)
            maxfd = fd;
    }
    void Setmaxfd()
    {
        maxfd = 0;
        for (int n = 0; n < CLIENT_NUM; n++)
        {
            switch (Getstatus(n))
            {
            case UNUSED:
                break;
            case LISTENING:
                Setmaxfd(listening_fd);
                break;
            case CONNECTED:
                Setmaxfd(connected_fd[n]);
                break;
            };
        }
        printf("maxfd:%d\n", maxfd);
    }
    void Setstatus(int num, enum Status s)
    {
        st[num] = s;
    }
    void Setstatus(enum Status s)
    {
        for (int num = 0; num < CLIENT_NUM; num++)
        {
            Setstatus(num, s);
        }
    }
    enum Status Getstatus(int num)
    {
        return st[num];
    }
};
class DataSend : public Main_server_user
{
  public:
    DataSend(int cl_num, int port_num) : Main_server_user(cl_num, port_num)
    {
    }
    ~DataSend() {}

  private:
    int buflen;
    char buf[1024];
    int sel;

  public:
    void server_func()
    {
        Listening_socket_init();
        Select();
    }
    void Select()
    {
        while (1)
        {
            memcpy(&fds, &readfds, sizeof(fd_set));

            if ((sel = select(maxfd + 1, &fds, NULL, NULL, &t_val)) < 0) //sel<0 error
            {
                perror("*** server: select ***");
                exit(1);
            }
            /* else if (sel == 0)
            { //time out
                puts("time out");
                break;
            }*/
            else
            {
                //printf("%d\n", maxfd);
                connect_server();
                data_send();
            }
        }
    }
    void data_send()
    {
        int i;
        /* データの送受信 */
        for (i = 0; i < CLIENT_NUM; i++)
        {
            if (Getstatus(i) == CONNECTED && FD_ISSET(connected_fd[i], &fds))
            {

                if ((buflen = recv(connected_fd[i], buf, sizeof(buf), 0)) <= 0)
                {
                    printf("no recv\n");
                    close_server(i);
                    continue;
                }
                printf(">>> Received (size:%d).\n", buflen);
                write(1, buf, buflen);
                printf("<<< Sending...\n");
                if (send(connected_fd[i], buf, buflen, 0) <= 0)
                {
                    puts("no send\n");
                    close_server(i);
                    continue;
                }
            }
        }
    }
};

class Sever_Hub : public Main_server
{
  public:
    Sever_Hub(int cl_num, int *port_num) : Main_server(cl_num, port_num)
    {
    }
    ~Sever_Hub() {}

  private:
    int buflen;
    char buf[1024];
    int sel;

  public:
    void server_func()
    {
        Listening_socket();
        Select();
    }
    void Select()
    {
        while (1)
        {
            memcpy(&fds, &readfds, sizeof(fd_set));

            if ((sel = select(maxfd + 1, &fds, NULL, NULL, &t_val)) < 0) //sel<0 error
            {
                perror("*** server: select ***");
                exit(1);
            }
            /* else if (sel == 0)
            { //time out
                puts("time out");
                break;
            }*/
            else
            {
                //printf("%d\n", maxfd);
                connect_server();
                data_send();
            }
        }
    }

    void data_send()
    {
        int i;
        /* データの送受信 */
        for (i = 0; i < CLIENT_NUM; i++)
        {
            if (Getstatus(i) == CONNECTED && FD_ISSET(connected_fd[i], &fds))
            {
                /*
                ユーザーからコマンドを受け取りinoutに送信
                コマンドのやりとりを中継
                過去ログ受け取り指定先に送信
                */

                switch (i)
                {
                case 0: //from user

                    break;
                case 1: //log
                    break;
                case 2: //log
                    break;
                case 3: //from inout
                    recv_client(i);
                    if (buflen <= 0)
                    {
                        continue;
                    }

                    break;
                }

                printf("<<< Sending...\n");
                if (send(connected_fd[i], buf, buflen, 0) <= 0)
                {
                    puts("no send\n");
                    close_server(i);
                    continue;
                }
            }
        }
    }
    void recv_client(int i)
    {
        if ((buflen = recv(connected_fd[i], buf, sizeof(buf), 0)) <= 0)
        {
            printf("no recv\n");
            close_server(i);
        }
        printf(">>> Received (size:%d).\n", buflen);
        write(1, buf, buflen);
    }
    int search_client(int p_num)
    {
        for (int i = 0; i < CLIENT_NUM; i++)
        {
            if (PORT[i] == p_num)
            {
                return i;
            }
        }
        return -1;
    }
};
int main(void)
{

    int client_num, *port_number_list, i;
    client_num = 5;
    port_number_list = new int[client_num];
    for (i = 0; i < client_num; i++)
        port_number_list[i] = 59630 + i;

    //Main_server server(client_num, port_number_list);
    DataSend server(client_num, port_number_list[0]);

    printf("Waiting for connections from a client.\n");
    server.server_func();
    delete[] port_number_list;
}