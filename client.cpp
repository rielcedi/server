/*
 * client.cpp
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef __linux
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#ifdef _WIN32
#include <winsock2.h>
/*
compile 
g++ -lwsock32 client.cpp -lws2_32 -o client
*/
#endif

class Client
{
  public:
    Client()
    {
        PORT = 0;
    }
    ~Client()
    {
        PORT = 0;
    }

  private:
    struct sockaddr_in addr;
    int fd;
    int buflen;
    char buf[1024];
    int PORT;
#ifdef _WIN32
    WSADATA wsaData;
#endif
  public:
    void make_socket(int port, char *ipaddr)
    {
        PORT = port;
#ifdef _WIN32
        WSAStartup(MAKEWORD(2, 0), &wsaData);
#endif
        /* ソケット作成 */
        if ((fd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("*** client: socket ***");
            exit(1);
        }
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = PF_INET;
        addr.sin_addr.s_addr = inet_addr(ipaddr);
        addr.sin_port = htons(PORT);
    }

    void client_connect()
    {

        /* ソケット接続要求 */
        if (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
            perror("*** client: connect ***");
            close(fd);
            exit(1);
        }
        printf("Connected to server.\n");
        printf("Input 'quit' to disconnect...\n");

        /* データの送受信 */
        while (1)
        {
            buflen = read(0, buf, sizeof(buf) - 1);
            buf[buflen] = '\0';
            printf("%s\n", buf);
            if (strcmp(buf, "quit\n") == 0)
            {
                break;
            }
            printf("<<< Sending...\n");
            if (send(fd, buf, buflen, 0) < 0)
            {
                break;
            }

            if ((buflen = recv(fd, buf, sizeof(buf), 0)) <= 0)
            {
                break;
            }
            printf(">>> Received.\n");
            write(1, buf, buflen);
        }

        /* ソケット切断 */
        exit(close(fd));
#ifdef _WIN32
        WSACleanup();
#endif
    }
};

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s <IP Address>\n", argv[0]);
        exit(1);
    }

    Client client;
    client.make_socket(59630, argv[1]);
    client.client_connect();
}
